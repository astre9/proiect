import React, { Component } from 'react'

class Product extends Component {
    render() {
        let { item } = this.props
        return (
            <div>
  		{item.toString()}
  		<input type="button" value="buy" onClick={() => this.props.onBuy(item.id)} />
      </div>
        )
    }
}

export default Product
