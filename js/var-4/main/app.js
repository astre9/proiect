function removeOrderItem(orderInfo, position) {
    if (Array.isArray(orderInfo.items) == false) {
        throw new Error('Items should be an array')
    }
    else {
        var goodProps = true;

        orderInfo.items.forEach((el) => {
            if (el.hasOwnProperty('quantity') == false || el.hasOwnProperty('price') == false) {
                goodProps = false;
            }
        })
        if (goodProps == false) {
            throw new Error('Malformed item')
        }
        else if (position < orderInfo.items.length) {
            throw new Error('Invalid position')
        }
        else {

            orderInfo.items = orderInfo.items.splice(position, 1);
            orderInfo.total = 0;
            orderInfo.items.forEach((el) => {
                orderInfo.total += el

            })
        }
    }
    return orderInfo;
}
const app = {
    removeOrderItem
};

module.exports = app;
